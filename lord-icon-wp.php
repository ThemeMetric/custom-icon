<?php
/**
* Plugin Name: Custom icon
* Plugin URI: https://shweb.me
* Description: Insert 500 custom icon.
* Version: 1.0.0
* Author: Icon
* Author URI: https://shweb.me
**/

// Exit if accessed directly.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require plugin_dir_path( __FILE__ ) . 'includes/class-wp-lord-icon.php';

/**
 * Begins execution of the plugin.
 */
function run_wp_loid_icon() {
	$plugin = new \LordIcon\WP_LordIcon();
	$plugin->run();
}
run_wp_loid_icon();